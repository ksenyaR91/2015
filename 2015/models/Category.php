<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	public static function getCategory()
	{
		$allCategory = self::find()->all();
		$allCategoryArray = ArrayHelper::
					map($allCategory, 'id', 'name');
		return $allCategoryArray;						
	}
	public static function getCategory1()
	{
		$Categorys = ArrayHelper::
					map(self::find()->all(), 'id', 'name');
		return $Categorys;						
	}
	

	public static function getAllCategorys()
	{
		$allCategorys = self::getCategory1();
		$allCategorys[-1] = 'All Categorys';
		$allCategorys = array_reverse ( $allCategorys, true );
		return $allCategorys;	
	}	
	
}
